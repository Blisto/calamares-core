# Maintainer: shivanandvp <shivanandvp@rebornos.org>

pkgname=calamares-core
pkgver=0.0.1
pkgrel=1
arch=('i686' 'x86_64' 'armv6h' 'armv7h')
pkgdesc='Calamares Installer for RebornOS. This is practically just bare Calamares. Please also install rebornos-calamares-configuration.'
license=('GPL3')
depends=('qt5-location' 
         'qt5-svg'
         'qt5-tools'
         'qt5-webengine'
         'qt5-xmlpatterns'         
         'qt5ct'
         'polkit-qt5'         
         'kconfig'
         'kcoreaddons'
         'kdbusaddons'
         'ki18n'    
         'kpackage'
         'kparts'     
         'kpmcore'
         'plasma-framework'        
         'boost'
         'boost-libs'
         'appstream-qt'
         'libatasmart'
         'libpwquality'
         'parted' 
         'solid'       
         'udisks2' 
         'yaml-cpp'
         'polkit'
         'ckbcomp')
optdepends=()
makedepends=('extra-cmake-modules'
             'git'
             'qt5-tools') 
provides=("${pkgname}")
conflicts=("${pkgname}")
source=()
replaces=('calamares'
          'calamares-core')

PROJECT_DIRECTORY="$(dirname -- "$(dirname -- "$(pwd)")")"
BUILD_DIRECTORY="$PROJECT_DIRECTORY/build"
RESOURCE_DIRECTORY="$PROJECT_DIRECTORY/scripts/packaging"
NUMBER_OF_PROCESSORS="$(nproc)"

prepare() {
    :
}

build() {     
    mkdir -p "$BUILD_DIRECTORY" && \
    cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -B "$BUILD_DIRECTORY" -S "$PROJECT_DIRECTORY" && \
    cmake --build "$BUILD_DIRECTORY" --parallel "$NUMBER_OF_PROCESSORS"
}

package() {
    # To bypass makepkg's dependency check during build-time
    depends+=('calamares-configuration'
               'calamares-branding')

    DESTDIR="${pkgdir}" cmake --install "$BUILD_DIRECTORY" # Install after setting an environment variable

    (
        cd "$RESOURCE_DIRECTORY" && \
        find . \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name '*.sh' \
            -exec \
                install -Dm 755 "{}" "${pkgdir}/usr/share/calamares/{}" \; \
            -exec \
                ln -s "/usr/share/calamares/{}" "${pkgdir}/usr/bin/{}" \;           
    )
    (
        cd "$RESOURCE_DIRECTORY" && \
        find . \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name '*.policy' \
            -exec \
                install -Dm 755 "{}" "${pkgdir}/usr/share/polkit-1/actions/{}" \;
    )
    (
        cd "$RESOURCE_DIRECTORY" && \
        find . \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name '*.desktop' \
            -exec \
                install -Dm 755 "{}" "${pkgdir}/usr/share/applications/{}" \;
    )
    (
        cd "$RESOURCE_DIRECTORY" && \
        find . \
            -mindepth 1 \
            -maxdepth 1 \
            -type f \
            -name '*.svg' \
            -exec \
                install -Dm 755 "{}" "${pkgdir}/usr/share/pixmaps/{}" \;
    )
}